const axios = require("axios");

const baseURL = 'https://api.instagram.com/v1';
const ACCESS_TOKEN = '926791915.dce3d38.2d7c9dee8b0c4699af0f82160e620199';
const createInstance = () => {
    return axios.create({
        baseURL
    });
}

const authenticate = (req, res, next) => {
    const instance = createInstance()
    Promise.all([
        instance.get(`/users/self?access_token=${ACCESS_TOKEN}`),
        instance.get(`/users/self/media/recent?access_token=${ACCESS_TOKEN}`)
    ])
        .then(results => {
            res.send({
                user: results[0].data,
                media: results[1].data
            });
            next();
        })
        .catch(err => {
            console.error(err);
            res.send(400);
            next();
        })
};

module.exports = authenticate;